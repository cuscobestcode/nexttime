const { gql } = require('apollo-server-express');

const typeDefs = gql`
   scalar Date
   type Transporte {
    modelo:String,
    marca: String,
    asientos: Int,
    anio: Int,
    tipo: String,
    categoria: String
    licencia: String
    matricula: String
   }
   
   type NotificacionD{
    detalles:String,
    modelo:String,
    marca: String,
    asientos: Int,
    anio: Int,
    tipo: String,
  }

  type NotificacionU{
    lat: Float,
    log: Float,
    direccion: String
  }
   

   type Subasta {
    _id: ID,
    comentario: String,
    precio: String,
    fecha: Date,
    usuario: Usuarios
   }

  
   type Usuarios {
    _id:ID,
    nombre: String,
    apellidos: String,
    correo: String,
    contrasenia: String,
    dni: String,
    transporte:Transporte,
    fechanacimiento: String,
    categoria: String
    licencia: String
   } 

   type Clientes {
    _id:ID,
    nombre: String,
    apellidos: String,
    correo: String,
    contrasenia: String,
    dni: String,
    transporte:Transporte,
   } 

   type Notificacions {
    _id:ID,
    cliente:Clientes,
    titulo: String,
    fecha: Date,
    descripcion: NotificacionD !,
    precio: Float,
    empieza: NotificacionU !,
    termina: NotificacionU !, 
    servicio: String,
    subasta: [Subasta]
   } 

   type Token{
     iv: String,
     content: String
   }

   type Login{
    token:Token, 
    status: Boolean
   }

   type Autenticate{
     status: Boolean,
     user: String
   }

   type Chat{
     de: Clientes,
     a: Usuarios,
     envio: String,
     mensage: String
   }



   input LoginInput{
    correo:String, 
    contrasenia: String
   }
   input TokenInput{
    iv: String,
    content: String
  }






  type Query {
    Usuarios: [Usuarios!]!,
    Clientes: [Clientes!]!,
    Notificacions: [Notificacions!]!,
    NotificacionsClient(input:TokenInput): [Notificacions!]!,
    Autenticate(input:TokenInput):Boolean,
    AutenticateC(input:TokenInput):Autenticate,
    GetUser(input:TokenInput):Usuarios,
    GwtOneNotification(notificacion: String): Notificacions,
    ChatClient(input:TokenInput):[Chat],
    GetPerfil(input:TokenInput): Usuarios
  }
  input TransporteInput{
    modelo:String,
    marca: String,
    asientos: Int,
    anio: Int,
    tipo: String,
    matricula: String,
    categoria: String
    licencia: String
  } 

  input NotificacionDescripcion{
    detalles:String,
    modelo:String,
    marca: String,
    asientos: Int,
    anio: Int,
     
  }
 

  input UsuarioInput{
    nombre: String
    apellidos: String
    correo: String
    contrasenia: String
    dni: String
    licencia: String
    fechanacimiento: String
    categoria: String
    transporte:TransporteInput!
  }
  

  input ClienteInput{
    nombre: String,
    apellidos: String,
    correo: String,
    contrasenia: String,
    dni: String
  }

  input NotificacionUbicacion{
    lat: Float,
    log: Float,
    direccion: String
  }

  input NotificacionInput{
    cliente:TokenInput,
    titulo: String,
    fecha:String,
    descripcion: NotificacionDescripcion,
    precio: Float,
    empieza: NotificacionUbicacion,
    termina: NotificacionUbicacion,
    servicio: String
  }

  input SubastaNotificacion{
    comentario: String,
    precio: Float,
    
  }

  input ChatInput{
    de: String,
    a: String,
    envio: String,
    mensage: String
  }



  type Mutation{
    
    crearUsuario(input: UsuarioInput):Int,
    crearCliente(input: ClienteInput):Boolean,
    createNotificacion(input: NotificacionInput):Int,
    createSubasta(notificacion:String,input: SubastaNotificacion,usuario:TokenInput):Int,
    Login(input:LoginInput):Login,
    LoginC(input:LoginInput):Login,
    VerificaClient(correo: String):Boolean,
    CreateMessage(input: ChatInput):Boolean,
    DeleteSubasta(notificacion:String,subasta:String):Boolean

  }

  

  type Subscription{
    newUsuario:Usuarios,
    newCliente:[Clientes],
    newNotificacion:Notificacions
    
  }
`;

module.exports = typeDefs;