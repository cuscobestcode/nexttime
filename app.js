const { ApolloServer } = require('apollo-server');
const { mongoConnect, schema, postgresConnect } = require('./utils/app');

const port = 4000;

mongoConnect().then(success => {
    console.log(success);
}).catch(error => {
    console.log(error);
});



const server = new ApolloServer({ schema: schema});

  server.listen().then(({ port, subscriptionsUrl }) => {
    console.log(`🚀 Server ready at ${port}`);
    console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
  });
