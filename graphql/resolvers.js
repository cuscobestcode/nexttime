const { PubSub } = require('apollo-server')
const { encrypt, decrypt } = require('../Assets/cripto');
const { Usuario,Cliente,Notificacion,Chat} = require('../mongo/mongoSchema')
const pubsub = new PubSub();
const bcrypt = require('bcrypt');
const { hash } = require('bcryptjs');
const saltRounds = 10;
const NEW_USUARIO = 'NEW_USUARIO';
const NEW_CLIENTE = 'NEW_CLIENTE';
const NEW_NOTIFICACION = 'NEW_NOTIFICACION';



const resolvers = {

    Query: {
        
        Usuarios: async () => {
            const data = await Usuario.find();
            if (!data.length) {
                return []
            } else {
                return data
            }
        },
        Clientes: async () => {
            const data = await Cliente.find();
            if (!data.length) {
                return []
            } else {
                return data
            }
        },
        Notificacions: async () => {
            const data = await Notificacion.find().sort({'fecha': -1});
         
            if (!data.length) {
                return []
            } else {
                return data
            }
        },
        NotificacionsClient: async (_,{input}) => {
            const id = decrypt(input)
            const data = await Notificacion.find({cliente:id}).sort({'fecha': -1}).populate({path:"subasta.usuario"});
            if (!data.length) {
                return []
            } else {
                return data
            }
        },
        

        Autenticate:async (_, {input}) => {
                    const id = decrypt(input)
                    const data = await Usuario.findById(id);
            
                    if(data){
                        return true
                    }else{
                        return false
                    }
                   
              
        },

        AutenticateC:async (_, {input}) => {
            
            const id = decrypt(input)
            const data = await Cliente.findById(id);
           
            if(data){
                return {
                    status:true,
                    user: data.correo
                }
            }else{
                return false
            }
           
      
},
        GetUser:async (_, {input}) => {
            const id = decrypt(input)
            const data = await Usuario.findById(id);
            if(data){
                return data
            }else{
                return false
            }
      
        },

        GwtOneNotification: async (_, {notificacion}) => {
            const data = await Notificacion.findById(notificacion).populate('cliente').populate({path:"subasta.usuario"});
            
            if(data){   
                return data
            }else{
                return null
            }
            
        },

        ChatClient: async (_, {input}) => {
            const id = decrypt(input)
            const data = await Chat.find({_id:id});
       
            if(data){
                return data
            }else{
                return []
            }
        },

        GetPerfil: async (_, {input}) => {
            const id = decrypt(input)
            const data = await Cliente.findById(id);
            if(data){
                return data
            }else{
                return []
            }
        }

        },

    Mutation: {
      
        crearUsuario: async (_, {input}) => {
            let {correo,contrasenia,apellidos,dni,nombre,licencia,fechanacimiento,categoria,transporte} = input
            const salt = bcrypt.genSaltSync(saltRounds);
            const hash = bcrypt.hashSync(contrasenia, salt);
            const newUsuario = new Usuario({correo,contrasenia:hash,apellidos,dni,nombre,licencia,fechanacimiento,categoria,transporte});
                const data = await newUsuario.save();
                if (data) {
                    return 1;
                } else {
                    return 0;
                }
            
        },
        crearCliente: async (_, { input }) => {
            let {contrasenia,apellidos,dni,nombre,correo} = input
            const salt = bcrypt.genSaltSync(saltRounds);
            const hash = bcrypt.hashSync(contrasenia, salt);
            const newCliente = new Cliente({contrasenia:hash,apellidos,dni,nombre,correo});
            const data = await newCliente.save();

            if (data) {
                pubsub.publish(NEW_CLIENTE,
                    {
                        newCliente: data
                    });
                return true;
            } else {
                return false;
            }
        },
        createNotificacion: async (_, { input }) => {
           
            let {cliente, titulo, fecha, descripcion,  precio, empieza,termina,servicio} = input
           cliente = decrypt(cliente)
            console.log(input)
            const newNotificacion = new Notificacion({cliente,titulo,fecha,descripcion,precio,empieza,termina,servicio});
            const data = await newNotificacion.save();

            if (data) {
                pubsub.publish(NEW_NOTIFICACION,
                    {
                        newNotificacion: data
                    });
                return 1;
            } else {
                return 0;
            }
        },


        createSubasta: async (_, { notificacion,input,usuario }) => {
            console.log(notificacion)
            console.log(input)
            console.log(usuario)
            let subasta ={
                comentario: input.comentario,
                precio: input.precio,
                usuario:  decrypt(usuario)
            }
            const data = await Notificacion.updateOne({_id:notificacion},{ $push: { subasta }});
            if (data) {
               
                return 1;
            } else {
                return 0;
            }
        },
        
        Login: async (_, {input}) => {
            let {contrasenia,correo} = input;
            const data = await Usuario.findOne({correo});
            
            if (data) {
                if(bcrypt.compareSync(contrasenia,data.contrasenia)){
                    const hash = encrypt(`${data._id}`)
                    return {
                        status: true,
                        token: hash
                    }
                }else{
                    return{
                        status: false
                    }
                }
            } else {
                return {
                    status: false
                }
            }
        },

        LoginC: async (_, {input}) => {
            let {contrasenia,correo} = input;
            const data = await Cliente.findOne({correo});
            
            if (data) {
                if(bcrypt.compareSync(contrasenia,data.contrasenia)){
                    const hash = encrypt(`${data._id}`)
                    return {
                        status: true,
                        token: hash
                    }
                }else{
                    return{
                        status: false
                    }
                }
            } else {
                return {
                    status: false
                }
            }
        },
        VerificaClient: async (_, {correo}) => {
            const data = await Cliente.findOne({correo});
            
            if (data) {
                 return true
            } else {
               return false
            }
        },
        CreateMessage: async (_, {input}) => {
            const newChat = new Chat(input);
            const data = await newChat.save();
            if (data) {
                 return true
            } else {
               return false
            }
        },

        //# ELIMINAR LA SUBASTA 
        DeleteSubasta: async (_, {notificacion,subasta}) => {
            const data = await Notificacion.updateOne({_id:notificacion},{ $pull:{subasta:{_id:subasta}}});
            if (data) {
                 return true
            } else {
               return false
            }
        }
       
    },

    Subscription: {
       
        newUsuario: {
            subscribe: () => pubsub.asyncIterator(NEW_USUARIO),
            resolve:(payload) => {
                // Manipulate and return the new value
                
                return payload.newUsuario
            },
        },


        newCliente: {
            subscribe: () => pubsub.asyncIterator(NEW_CLIENTE),
            resolve:(payload) => {
                // Manipulate and return the new value
                
                return payload.newCliente
            },
        },
        newNotificacion: {
            subscribe: () => pubsub.asyncIterator(NEW_NOTIFICACION),
            resolve:(payload) => {
                // Manipulate and return the new value
                
                return payload.newNotificacion
            },
        },
        
    }

};


module.exports = resolvers;