const { Schema, model } = require('mongoose');


// schemas
const Usuario = new Schema({
    nombre: String,
    apellidos: String,
    correo: String,
    contrasenia: String,
    dni: String,
    transporte: {
        licencia: String,
        modelo:String,
        marca: String,
        asientos: Number,
        anio: Number,
        tipo: String,
        matricula: String,
        categoria: String  
    },

    licencia: String,
    fechanacimiento: String,
    categoria: String  
})



const Cliente = new Schema({
    nombre: String,
    apellidos: String,
    correo: String,
    contrasenia: String,
    dni: String,
    transporte: {
        licencia: String,
        modelo:String,
        marca: String,
        asientos: Number,
        anio: Number,
        tipo: String,
        matricula: String,
        categoria: String  
    }
})

const Notificacion = new Schema({
    cliente:{
        type: Schema.Types.ObjectId, ref: 'Cliente'
    },
    titulo: String,
    fecha:{ type: Date, default: Date.now },
    descripcion: {
        detalles:String,
        modelo:String,
        marca: String,
        asientos: Number,
        anio: Number,
        tipo: String,
    },
    precio: Number,
    empieza: {
        lat: Number,
        log: Number,
        direccion: String
    },
    termina: {
        lat: Number,
        log: Number,
        direccion: String
    },
    servicio:String,
    subasta:[{
        comentario: String,
        precio:  Number,
        fecha: { type: Date, default: Date.now },
        usuario: {
            type: Schema.Types.ObjectId, ref: 'Cliente'
        },
        default: {}
    }],
    chat:[{
        mensage: String,
        hora:   { type: Date, default: Date.now },
        stado: { type:  Schema.Types.Boolean },
        default: {}
    }]
})

const Chat = new Schema({
    de:{
        type: Schema.Types.ObjectId, ref: 'Cliente'
    },
    a:{
        type: Schema.Types.ObjectId, ref: 'Cliente'
    },
    envio: { type: Date, default: Date.now },
    mensage:String,
})


//export models
module.exports.Usuario = model('Usuario', Usuario)
module.exports.Cliente = model('Cliente', Cliente)
module.exports.Notificacion = model('Notificacion', Notificacion)
module.exports.Chat = model('Chat', Chat)
